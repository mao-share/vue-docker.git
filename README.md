# vue-docker

<a href='https://gitee.com/mao-share/vue-docker'><img src='https://gitee.com/mao-share/vue-docker/widgets/widget_5.svg' alt='Fork me on Gitee'></img></a>

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

