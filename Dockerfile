# build stage
FROM node-cnpm:last as build-stage
WORKDIR /app
COPY package*.json ./
COPY . .
RUN rm -f .env.*

ENV VUE_APP_TEST_VALUE="My test Value"
RUN cnpm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]